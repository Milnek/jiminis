package sio.lacol.bll;

import sio.lacol.bo.Insecte;
import sio.lacol.bo.Provenance;

import java.util.ArrayList;
import java.util.List;

public class InsecteController {

    private List<Insecte> lesInsectes;

    private static InsecteController instanceCtrl;

    public static synchronized InsecteController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new InsecteController();
        }
        return instanceCtrl;
    }

    public InsecteController(){
        this.lesInsectes = new ArrayList<>();
    }

    public List<Insecte> getAll(){
        return this.lesInsectes;
    }

    public void save(String nom, Provenance provenance, float prixAuKilo) {
        lesInsectes.add(new Insecte(nom, provenance, prixAuKilo));
    }
}
