package sio.lacol.bll;

import sio.lacol.bo.Provenance;

import java.util.ArrayList;
import java.util.List;

public class ProvenanceController {

    private List<Provenance> lesProvenances;

    private static ProvenanceController instanceCtrl;

    public static synchronized ProvenanceController getInstance(){
        if(instanceCtrl == null){
            instanceCtrl = new ProvenanceController();
        }
        return instanceCtrl;
    }

    public ProvenanceController(){
        lesProvenances = new ArrayList<>();
        lesProvenances.add(new Provenance("Europe"));
        lesProvenances.add(new Provenance("France"));
    }

    public List<Provenance> getAll(){
        return this.lesProvenances;
    }

    public List<String> getAllLibelles(){
        List<String> lesLibelles = new ArrayList<>();
        for (Provenance prov : lesProvenances) {
            lesLibelles.add(prov.getLibelle());
        }
        return lesLibelles;
    }

    public Provenance getProvenanceByLibelle(String libelle){
        Provenance provenance = new Provenance(libelle);

        for (Provenance prov : lesProvenances) {
            if (prov.getLibelle().equals(libelle)){
                provenance = prov;
            }
        }
        return provenance;
    }
}
