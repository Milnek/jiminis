package sio.lacol.bo;

public class Provenance {

    private String libelle;

    public Provenance(String libelle)
    {
        this.libelle = libelle;
    }

    public String getLibelle()
    {
        return this.libelle;
    }

    @Override
    public String toString()
    {
        return this.libelle;
    }
}
