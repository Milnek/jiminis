package sio.lacol.bo;

public class Insecte {
    private String nom;
    private Provenance provenance;
    private float prixAuKilo;

    public Insecte(String nom, Provenance provenance, float prixAuKilo){
        this.nom = nom;
        this.provenance = provenance;
        this.prixAuKilo = prixAuKilo;
    }
    public String getNom()
    {
        return this.nom;
    }
    public Provenance getProvenance()
    {
        return this.provenance;
    }
    public float getPrixAuKilo()
    {
        return this.prixAuKilo;
    }
    @Override
    public String toString()
    {
        return this.nom + " (" + this.provenance + ") à " + this.prixAuKilo + "€/kg";
    }
}
