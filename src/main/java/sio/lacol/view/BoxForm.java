package sio.lacol.view;

import sio.lacol.bll.InsecteController;
import sio.lacol.bo.Insecte;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;

public class BoxForm extends JDialog {
    public BoxForm(Window owner) {
        super(owner);

        setPreferredSize(new Dimension(300, 300));
        initComponents();
    }

    private void initComboboxInsecte(){
        List<Insecte> lesInsectes = InsecteController.getInstance().getAll();
        if (lesInsectes != null) {
            DefaultComboBoxModel<Insecte> modelCombo = new DefaultComboBoxModel<>();
            for (Insecte insecte : lesInsectes) {
                modelCombo.addElement(insecte);
            }
            //rattachement du DefaultComboBoxModel au composant graphique ComboBox
            insecteComboBox.setModel(modelCombo);
        }
    }

    private void saveButtonActionPerformed(ActionEvent e) {
        // TODO add your code here
        System.out.print("TODO add your code");
    }
    private void closeButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void thisWindowGainedFocus(WindowEvent e) {
        initComboboxInsecte();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        insecteLabel = new JLabel();
        insecteComboBox = new JComboBox();
        poidsLabel = new JLabel();
        poidsTextField = new JTextField();
        grammeLabel = new JLabel();
        prixLabel = new JLabel();
        prixCalculéLabel = new JLabel();
        euroLabel = new JLabel();
        buttonBar = new JPanel();
        saveButton = new JButton();
        closeButton = new JButton();

        //======== this ========
        addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                thisWindowGainedFocus(e);
            }
        });
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {93, 202, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 32, 0, 0, 36, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0E-4};

                //---- insecteLabel ----
                insecteLabel.setText("Insecte");
                contentPanel.add(insecteLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(insecteComboBox, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- poidsLabel ----
                poidsLabel.setText("Poids");
                contentPanel.add(poidsLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(poidsTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- grammeLabel ----
                grammeLabel.setText("g");
                contentPanel.add(grammeLabel, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- prixLabel ----
                prixLabel.setText("Prix");
                contentPanel.add(prixLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 10, 5, 5), 0, 0));

                //---- prixCalculéLabel ----
                prixCalculéLabel.setText("prix calcul\u00e9");
                contentPanel.add(prixCalculéLabel, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- euroLabel ----
                euroLabel.setText("\u20ac");
                contentPanel.add(euroLabel, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- saveButton ----
                saveButton.setText("Save");
                saveButton.addActionListener(e -> saveButtonActionPerformed(e));
                buttonBar.add(saveButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(e -> closeButtonActionPerformed(e));
                buttonBar.add(closeButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel insecteLabel;
    private JComboBox insecteComboBox;
    private JLabel poidsLabel;
    private JTextField poidsTextField;
    private JLabel grammeLabel;
    private JLabel prixLabel;
    private JLabel prixCalculéLabel;
    private JLabel euroLabel;
    private JPanel buttonBar;
    private JButton saveButton;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
