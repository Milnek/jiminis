package sio.lacol.view;

import sio.lacol.bll.ProvenanceController;
import sio.lacol.bll.InsecteController;
import sio.lacol.bo.Provenance;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;


public class InsecteForm extends JDialog {

    public InsecteForm(Window owner) {
        super(owner);

        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(300, 300));

        setTitle("Création d'un nouvel insecte ");
        initComponents();

        initComboProvenance();

    }

    private void initComboProvenance() {
        //!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

        // création d'un model à partir de la liste d'objets Provenance
        DefaultComboBoxModel<Provenance> modelCombo = new DefaultComboBoxModel<>();

        List<Provenance> provenances = ProvenanceController.getInstance().getAll();

        for (Provenance prov : provenances){
            modelCombo.addElement(prov);
        }


        //rattachement du DefaultComboBoxModel au composant graphique ComboBox
        provenanceComboBox.setModel(modelCombo);

    }

    private void saveButtonActionPerformed(ActionEvent e) {
        String libelle = provenanceComboBox.getSelectedItem().toString();
        Provenance provenance = ProvenanceController.getInstance().getProvenanceByLibelle(libelle);
        Float prixAuKilo =  Float.parseFloat(prixTextField.getText());
        InsecteController.getInstance().save(nomTextField.getText(), provenance, prixAuKilo);
        this.dispose();
    }
    private void closeButtonActionPerformed(ActionEvent e) {
        this.dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        nomLabel = new JLabel();
        nomTextField = new JTextField();
        provenanceLabel = new JLabel();
        provenanceComboBox = new JComboBox();
        prixLabel = new JLabel();
        prixTextField = new JTextField();
        euroLabel = new JLabel();
        buttonBar = new JPanel();
        saveButton = new JButton();
        closeButton = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridBagLayout());
                ((GridBagLayout)contentPanel.getLayout()).columnWidths = new int[] {93, 202, 0, 0};
                ((GridBagLayout)contentPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 32, 0, 0, 36, 0};
                ((GridBagLayout)contentPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0, 1.0E-4};
                ((GridBagLayout)contentPanel.getLayout()).rowWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0E-4};

                //---- nomLabel ----
                nomLabel.setText("Nom");
                contentPanel.add(nomLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(nomTextField, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- provenanceLabel ----
                provenanceLabel.setText("Provenance");
                contentPanel.add(provenanceLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 10, 5, 5), 0, 0));
                contentPanel.add(provenanceComboBox, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- prixLabel ----
                prixLabel.setText("Prix au kilo");
                contentPanel.add(prixLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                contentPanel.add(prixTextField, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- euroLabel ----
                euroLabel.setText("\u20ac");
                contentPanel.add(euroLabel, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- saveButton ----
                saveButton.setText("Save");
                saveButton.addActionListener(e -> saveButtonActionPerformed(e));
                buttonBar.add(saveButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- closeButton ----
                closeButton.setText("Close");
                closeButton.addActionListener(e -> closeButtonActionPerformed(e));
                buttonBar.add(closeButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel nomLabel;
    private JTextField nomTextField;
    private JLabel provenanceLabel;
    private JComboBox provenanceComboBox;
    private JLabel prixLabel;
    private JTextField prixTextField;
    private JLabel euroLabel;
    private JPanel buttonBar;
    private JButton saveButton;
    private JButton closeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
